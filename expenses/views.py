from django.shortcuts import render
from .models import Expenses
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from .serializers import ExpensesSerializer
from rest_framework import permissions
from .permissions import IsOwner

class ExpensesListAPIView(ListCreateAPIView):
    serializer_class = ExpensesSerializer 
    queryset = Expenses.objects.all()
    permissions = (permissions.IsAuthenticated, )

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user)

    def perform_create(self, serializer):
        return serializer.save(owner = self.request.user)

class ExpensesDetailsAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = ExpensesSerializer 
    queryset = Expenses.objects.all()
    permissions = (permissions.IsAuthenticated, IsOwner)
    lookup_fields = "id"

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user)

    def perform_create(self, serializer):
        return serializer.save(owner = self.request.user)
